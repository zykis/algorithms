var intArr:[Int] = [-2, 3, -6, 10, 22, 0, -2, 0, 12, 36]
var strArr:[String] = ["Hey", "Dude", "How", "Are", "You"]

func printArray<T>(arr: inout [T]) {
	for el in arr {
		print("el: \(el)")
	}
}

func bubbleSort<T>(arr: inout [T], ascending: Bool = true) where T : Comparable {
	print("before sort:")
	printArray(arr: &arr)
	for i in 0...arr.count - 2 {
		for j in 0...arr.count - 2 - i {
			if ascending ? arr[j] > arr[j+1] : arr[j] < arr[j+1] {
				let tmp = arr[j]
				arr[j] = arr[j+1]
				arr[j+1] = tmp
			}
		}
		
	}
	print("after sort:")
	printArray(arr: &arr)
}

func coctailSort<T>(arr: inout [T], ascending: Bool = true) where T : Comparable {
	print("before sort:")
	printArray(arr: &arr)
	for i in 0...arr.count - 2 {
		for j in 0...arr.count - 2 - i {
			if ascending ? arr[j] > arr[j+1] : arr[j] < arr[j+1] {
				let tmp = arr[j]
				arr[j] = arr[j+1]
				arr[j+1] = tmp
			}
		}
		
	}
	print("after sort:")
	printArray(arr: &arr)
}

coctailSort(arr:&intArr, ascending:true)
coctailSort(arr:&strArr, ascending:true)

